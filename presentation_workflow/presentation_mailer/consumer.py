import json
import pika
import django
import os
import sys
import time
from django.core.mail import send_mail
from pika.exceptions import AMQPConnectionError


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()


while True:
    try:
        def process_approval(ch, method, properties, body):
            print("  Received %r" % body)
            var_name = json.loads(body)
            if var_name['status'] == 'APPROVED':
                send_mail(
                    recipient_list=[var_name['presenter_email']],
                    from_email='admin@conference.go',
                    subject='Your presentation has been accepted',
                    message=f"{var_name['presenter_name']}, we're happy to tell you that your presentation {(var_name['title'])} has been accepted.",
                    fail_silently=False,
                )


        def process_rejection(ch, method, properties, body):
            print("  Received %r" % body)
            var_name = json.loads(body)
            if var_name['status'] == 'REJECTED':
                send_mail(
                    recipient_list=[var_name['presenter_email']],
                    from_email='admin@conference.go',
                    subject='Your presentation has been rejected',
                    message=f"{var_name['presenter_name']}, we're sad to tell you that your presentation {(var_name['title'])} has been rejected.",
                    fail_silently=False,
                )


        def main():
            parameters = pika.ConnectionParameters(host='rabbitmq')
            connection = pika.BlockingConnection(parameters)
            channel = connection.channel()
            channel.queue_declare(queue='presentation_approvals')
            channel.queue_declare(queue='presentation_rejections')
            channel.basic_consume(
                queue='presentation_approvals',
                on_message_callback=process_approval,
                auto_ack=True,
            )
            channel.basic_consume(
                queue='presentation_rejections',
                on_message_callback=process_rejection,
                auto_ack=True,
            )
            print(' [*] Waiting for messages. To exit press CTRL+C')
            channel.start_consuming()


        if __name__ == '__main__':
            try:
                main()
            except KeyboardInterrupt:
                print('Interrupted')
                try:
                    sys.exit(0)
                except SystemExit:
                    os._exit(0)
    except AMQPConnectionError:
        print("Could not connect to RabbitMQ")
        time.sleep(2.0)
