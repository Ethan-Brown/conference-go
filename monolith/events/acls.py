from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json


def get_photo(city, state):
    # Create a dictionary for the headers to use in the request
    headers = {"Authorization": PEXELS_API_KEY}
    # Create the URL for the request with the city and state
    url = f"https://api.pexels.com/v1/search?query={city}+{state}&per_page=1"
    # Make the request
    request = requests.get(url, headers=headers)
    # Parse the JSON response
    response = json.loads(request.content)
    # Return a dictionary that contains a `picture_url` key and
    #   one of the URLs for one of the pictures in the response
    pexel_dict = {"picture_url": response["photos"][0]["url"]}
    return pexel_dict


def get_weather_data(city, state):
    # Create the URL for the geocoding API with the city and state
    latlon_url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},1&limit=&appid={OPEN_WEATHER_API_KEY}"
    # Make the request
    request = requests.get(latlon_url)
    # Parse the JSON response
    response = json.loads(request.content)
    # Get the latitude and longitude from the response
    lat = response[0]["lat"]
    lon = response[0]["lon"]

    # Create the URL for the current weather API with the latitude
    #   and longitude
    weather_url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}"
    # Make the request
    weather_request = requests.get(weather_url)
    # Parse the JSON response
    weather_response = json.loads(weather_request.content)
    # Get the main temperature and the weather's description and put
    #   them in a dictionary
    weather_dict = {
        "temperature": weather_response["main"]["temp"],
        "description": weather_response["weather"][0]["description"],
    }
    # Return the dictionary
    try:
        return weather_dict
    except:
        return {"weather": None}
